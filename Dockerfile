FROM maven:3.9.5-eclipse-temurin-17-alpine AS build
WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY . .
RUN mvn package

FROM eclipse-temurin:17-jre-alpine
WORKDIR /app

COPY --from=build /app/target/*-jar-with-dependencies.jar ./player.jar

ENTRYPOINT ["java", "-jar", "player.jar"]
