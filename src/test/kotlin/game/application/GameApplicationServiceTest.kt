package game.application

import core.EventTestFactory
import core.restadapter.GameServiceRESTAdapter
import game.domain.Game
import game.domain.GameRepository
import game.domain.GameStatus
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import player.application.PlayerApplicationService
import java.util.*

@ExtendWith(MockitoExtension::class)
class GameApplicationServiceTest {

    @Mock
    lateinit var gameRepository: GameRepository

    @Mock
    lateinit var gameServiceRESTAdapter: GameServiceRESTAdapter

    @Mock
    lateinit var playerApplicationService: PlayerApplicationService

    @InjectMocks
    lateinit var gameApplicationService: GameApplicationService

    @BeforeEach
    fun setup() {
        Mockito.reset(gameRepository, gameServiceRESTAdapter, playerApplicationService)
    }

    @Test
    fun `joinAndSaveNewGame saves the game when player joins`() {
        // Given
        val gameStatusEvent = EventTestFactory.createGameStatusEvent()
        runBlocking {

        Mockito.`when`(playerApplicationService.joinGame(gameStatusEvent.payload.gameId)).thenReturn(true)


        // When
        gameApplicationService.joinAndSaveNewGame(gameStatusEvent)

        // Then
        Mockito.verify(gameRepository).save(any())
        }
    }

    @Test
    fun `changeGameStatus updates game status if game exists`() {
        // Given
        val gameId = UUID.randomUUID()
        val game = Game(gameId, GameStatus.CREATED, 0)
        Mockito.`when`(gameRepository.findById(gameId)).thenReturn(game)
        // When
        gameApplicationService.changeGameStatus(gameId, GameStatus.STARTED)
        // Then
        Assertions.assertEquals(GameStatus.STARTED, game.gameStatus)
        Mockito.verify(gameRepository).save(game)
    }

    @Test
    fun `queryActiveGame returns null if there are multiple active games`() {
        // Given
        val games =
            listOf(Game(UUID.randomUUID(), GameStatus.CREATED, 0), Game(UUID.randomUUID(), GameStatus.STARTED, 0))
        Mockito.`when`(gameRepository.findAllByGameStatusBetween(GameStatus.CREATED, GameStatus.STARTED)).thenReturn(games)

        // When
        val result = gameApplicationService.queryActiveGame()

        // Then
        Assertions.assertNull(result)
    }

    @Test
    fun `roundStarted updates current round number for active game`() {
        // Given
        val game = Game(UUID.randomUUID(), GameStatus.CREATED, 0)
        Mockito.`when`(gameRepository.findAllByGameStatusBetween(GameStatus.CREATED, GameStatus.STARTED)).thenReturn(listOf(game))

        // When
        gameApplicationService.roundStarted(2)

        // Then
        assertEquals(2, game.currentRoundNumber)
        Mockito.verify(gameRepository).save(game)
    }

}