package core.domainprimitives.command

import core.domainprimitives.purchasing.Capability
import core.domainprimitives.purchasing.CapabilityType
import core.domainprimitives.purchasing.ItemTypeDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class CommandTest {

    private val playerId = UUID.randomUUID()
    private val robotId = UUID.randomUUID()
    private val planetId = UUID.randomUUID()
    private val targetId = UUID.randomUUID()

    @Test
    fun `test createMove command`() {
        val command = Command.createMove(robotId, planetId, playerId)
        assertEquals(CommandType.MOVEMENT, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
        assertEquals(planetId, command.commandObject.planetId)
    }

    @Test
    fun `test createItemPurchase command`() {
        val command = Command.createItemPurchase(playerId, robotId, ItemTypeDto.ENERGY_RESTORE, 1)
        assertEquals(CommandType.BUYING, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
        assertEquals(ItemTypeDto.ENERGY_RESTORE.name, command.commandObject.itemName)
        assertEquals(1, command.commandObject.itemQuantity)
    }

    @Test
    fun `test createRobotPurchase command`() {
        val command = Command.createRobotPurchase(playerId, 5)
        assertEquals(CommandType.BUYING, command.commandType)
        assertEquals("ROBOT", command.commandObject.itemName)
        assertEquals(5, command.commandObject.itemQuantity)
    }

    @Test
    fun `test createRobotUpgrade command`() {
        val capability = Capability.forTypeAndLevel(CapabilityType.MINING, 1)
        val command = Command.createRobotUpgrade(playerId, capability, robotId)
        assertEquals(CommandType.BUYING, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
        assertEquals(capability.nextLevel().toStringForCommand(), command.commandObject.itemName)
        assertEquals(1, command.commandObject.itemQuantity)
    }

    @Test
    fun `test createRobotRegeneration command`() {
        val command = Command.createRobotRegeneration(playerId, robotId)
        assertEquals(CommandType.REGENERATE, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
    }

    @Test
    fun `test createMining command`() {
        val command = Command.createMining(playerId, robotId, planetId)
        assertEquals(CommandType.MINING, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
        assertEquals(planetId, command.commandObject.planetId)
    }

    @Test
    fun `test createSellInventory command`() {
        val command = Command.createSellInventory(playerId, robotId)
        assertEquals(CommandType.SELLING, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
    }

    @Test
    fun `test createBattleCommand`() {
        val command = Command.createBattleCommand(playerId, robotId, targetId)
        assertEquals(CommandType.BATTLE, command.commandType)
        assertEquals(robotId, command.commandObject.robotId)
        assertEquals(targetId, command.commandObject.targetId)
    }

    @Test
    fun `test toString`() {
        val command = Command.createMove(robotId, planetId, playerId)
        val expectedString = "MOVEMENT R:${robotId.toString().take(4)} P:${planetId.toString().take(4)} I:null"
        assertEquals(expectedString, command.toString())
    }
}
