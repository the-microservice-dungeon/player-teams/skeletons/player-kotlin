package player.domain

import config.AppConfig
import core.domainprimitives.purchasing.Money
import java.util.*

class Player(
    val playerId: UUID,
    var gameId : UUID? = null,
    val name: String = AppConfig.playerName,
    val email: String = AppConfig.playerEmail,
    val playerExchange: String = "player-${AppConfig.playerName}",
    val playerQueue: String = "player-${AppConfig.playerName}",
    var money : Money = Money.zero()
)