package player.domain

import core.CrudRepository
import java.util.*

interface PlayerRepository : CrudRepository<Player, UUID> {

    fun findPlayerByGameId(gameId: UUID): Player?
}