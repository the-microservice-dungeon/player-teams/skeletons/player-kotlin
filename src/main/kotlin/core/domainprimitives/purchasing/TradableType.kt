package core.domainprimitives.purchasing

import kotlinx.serialization.Serializable

@Serializable
enum class TradableType {
    UPGRADE,
    ITEM,
    RESOURCE,
    RESTORATION,
}