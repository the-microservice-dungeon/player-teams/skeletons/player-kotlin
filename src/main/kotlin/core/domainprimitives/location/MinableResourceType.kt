package core.domainprimitives.location

import kotlinx.serialization.Serializable

@Serializable
enum class MinableResourceType {
    COAL,
    IRON,
    GEM,
    GOLD,
    PLATIN;
}
