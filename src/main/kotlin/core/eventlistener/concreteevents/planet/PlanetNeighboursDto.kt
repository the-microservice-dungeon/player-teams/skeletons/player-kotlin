@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.planet

import core.domainprimitives.location.CompassDirection
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
data class PlanetNeighboursDto(
    private val id: UUID,
    private val direction: CompassDirection
)

