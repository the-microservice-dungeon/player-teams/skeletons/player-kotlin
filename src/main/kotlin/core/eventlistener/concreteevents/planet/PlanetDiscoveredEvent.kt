@file:UseSerializers(UUIDSerializer::class)

package core.eventlistener.concreteevents.planet

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
import kotlin.collections.ArrayList

@Serializable
@SerialName(EventType.PLANET_DISCOVERED)
data class PlanetDiscoveredEvent(
    override val header: GameEventHeader,
    val payload: PlanetDiscoveredPayload
) : GameEvent()
@Serializable
data class PlanetDiscoveredPayload(
    val planetId: UUID,
    val movementDifficulty: Int,
    val neighbours: ArrayList<UUID>,
    val resource: PlanetResourceDto
)