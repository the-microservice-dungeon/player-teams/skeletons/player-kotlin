@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.game

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import game.domain.GameStatus
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*

@Serializable
@SerialName(EventType.GAME_STATUS)
data class GameStatusEvent (
    override val header: GameEventHeader,
    val payload: GameStatusPayload
) : GameEvent()

@Serializable
data class GameStatusPayload (
    val gameId: UUID,
    val gameworldId : UUID? = null,
    val status : GameStatus
)