package core.eventlistener.concreteevents.game

import kotlinx.serialization.Serializable

@Serializable
data class ImpreciseTimingPredictions(
    val roundStart : String,
    val commandInputEnd : String,
    val roundEnd : String
)
