package core.eventlistener.concreteevents.robot.reveal

import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName(EventType.ROBOTS_REVEALED)
data class RobotsRevealedEvent(
    override val header: GameEventHeader,
    val payload: RobotsRevealedPayload
) : GameEvent()
@Serializable
data class RobotsRevealedPayload(
    private val robots : ArrayList<RobotsRevealedDto>
)