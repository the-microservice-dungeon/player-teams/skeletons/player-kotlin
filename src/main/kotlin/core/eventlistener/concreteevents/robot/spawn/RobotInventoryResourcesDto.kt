package core.eventlistener.concreteevents.robot.spawn

import kotlinx.serialization.Serializable

@Serializable
data class RobotInventoryResourcesDto(
    private val coal : Int,
    private val iron : Int,
    private val gem : Int,
    private val gold : Int,
    private val platin : Int,
)
