@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.move

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import java.util.*
@Serializable
data class RobotMovedPlanetDto(
    private val id : UUID,
    private val movementDifficulty: Int,
)
