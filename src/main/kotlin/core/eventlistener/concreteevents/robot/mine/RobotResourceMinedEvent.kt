@file:UseSerializers(UUIDSerializer::class)
package core.eventlistener.concreteevents.robot.mine

import core.domainprimitives.location.MinableResource
import core.domainprimitives.location.MinableResourceType
import core.eventlistener.EventType
import core.eventlistener.GameEvent
import core.eventlistener.GameEventHeader
import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.slf4j.LoggerFactory
import java.util.*

@Serializable
@SerialName(EventType.ROBOT_RESOURCE_MINED)
data class RobotResourceMinedEvent(
    override val header: GameEventHeader,
    val payload : RobotResourceMinedPayload
) : GameEvent() {
    private val logger = LoggerFactory.getLogger(javaClass)
    fun minedResourceAsDomainPrimitive(): MinableResource? {
        var minedResourceAsDomainPrimitive: MinableResource? = null
        try {
            minedResourceAsDomainPrimitive = MinableResource.fromTypeAndAmount(
                MinableResourceType.valueOf(payload.minedResource.uppercase()), payload.minedAmount
            )
        } catch (e: Exception) {
            logger.debug("Could not convert minedResource to MineableResource: " + e.message)
        }
        return minedResourceAsDomainPrimitive
    }

}

@Serializable
data class RobotResourceMinedPayload(
    val robotId : UUID,
    val minedAmount : Int,
    val minedResource : String,
    val resourceInventory : RobotResourceInventoryDto
)
