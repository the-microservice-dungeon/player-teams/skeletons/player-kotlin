package core.eventlistener

import DungeonPlayerRuntimeException

class RabbitMQNotAvailableException(message: String) : DungeonPlayerRuntimeException(message)