package core.eventlistener

import DungeonPlayerRuntimeException

class GameEventTypeDoesNotExistException(message : String = "Unknown Type in Header"): DungeonPlayerRuntimeException(message)