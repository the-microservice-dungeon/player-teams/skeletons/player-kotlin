package core.eventlistener.serializers

import core.eventlistener.GameEvent
import core.eventlistener.GameEventTypeDoesNotExistException
import io.github.classgraph.ClassGraph
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.serializer
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation


object GameEventSerializer : JsonContentPolymorphicSerializer<GameEvent>(GameEvent::class) {
    private val eventsPackageName = "core.eventlistener.concreteevents"

    val typeToClassMapping: Map<String, KClass<out GameEvent>> = ClassGraph()
        .enableClassInfo()
        .acceptPackages(eventsPackageName)
        .scan()
        .getSubclasses(GameEvent::class.qualifiedName)
        .map { Class.forName(it.name).kotlin }
        .filterIsInstance<KClass<out GameEvent>>()
        .associateBy { it.findAnnotation<SerialName>()?.value ?: throw NoSerialNameAnnotationForEventException("Missing @SerialName annotation for Event Class ${it.simpleName}") }


    @OptIn(InternalSerializationApi::class)
    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<GameEvent> {
        val type = element.jsonObject["header"]!!.jsonObject["type"]!!.jsonPrimitive.content
        val eventClass = typeToClassMapping[type] ?: throw GameEventTypeDoesNotExistException("Deserialization error. No event class found for type: $type in package: $eventsPackageName\n Available types: ${typeToClassMapping.keys} \n Event looked like this :\n${element.jsonObject}")
        return eventClass.serializer()
    }
}
