@file:UseSerializers(UUIDSerializer::class)
package game.domain

import core.eventlistener.serializers.UUIDSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers
import org.slf4j.LoggerFactory
import java.util.*
@Serializable
class Game (
    val gameId : UUID,
    var gameStatus: GameStatus,
    var currentRoundNumber : Int,
) {
    private val logger = LoggerFactory.getLogger(javaClass)
}
