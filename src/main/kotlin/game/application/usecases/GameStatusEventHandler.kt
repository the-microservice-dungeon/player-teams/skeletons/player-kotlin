package game.application.usecases

import core.eventlistener.EventHandler
import core.eventlistener.concreteevents.game.GameStatusEvent
import game.application.GameApplicationService
import game.domain.GameStatus
import org.slf4j.LoggerFactory

class GameStatusEventHandler(
    private val gameApplicationService: GameApplicationService,
) : EventHandler<GameStatusEvent> {
    private val logger = LoggerFactory.getLogger(javaClass)
    override suspend fun handle(event: GameStatusEvent) {
        logger.info("Handling GameStatus event for game: ${event.payload.gameId}")
        when(event.payload.status){
            GameStatus.CREATED -> gameApplicationService.joinAndSaveNewGame(event)
            GameStatus.STARTED -> {
                logger.info("Game ${event.payload.gameId} started")
                gameApplicationService.changeGameStatus(
                    gameId = event.payload.gameId,
                    gameStatus = event.payload.status
                )
            }
            GameStatus.FINISHED -> {
                logger.info("Game ${event.payload.gameId} finished")
                gameApplicationService.changeGameStatus(
                    gameId = event.payload.gameId,
                    gameStatus = event.payload.status
                )
            }
        }
    }



}
